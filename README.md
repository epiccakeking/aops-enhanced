# AoPS Enhanced
Notice: This is a work in progress rewrite so things may break/change.

## To-do

- [X] Replace flyouts with browser notifications
- [X] Quotes
- [X] Click post number to copy the url of the post
- [ ] Moderators can edit in locked topics
- [X] Moderators can moderate topics from the feed
- [ ] Dark themes
- [ ] Add custom tags to autotagging
- [ ] Read messages deleted while on topic
- [ ] Filter out threads with titles matching custom phrases
